import java.util.ArrayList;
import java.util.List;

public class ExemploProxy {
    public static void main(String[] args) {
        List<IPessoa> pessoas = new ArrayList<IPessoa>();

        pessoas.add(new PessoaProxy("A"));
        pessoas.add(new PessoaProxy("B"));
        pessoas.add(new PessoaProxy("C"));

        System.out.println("Nome: " + pessoas.get(0).getNome());
        // busca do banco de dados
        System.out.println("Nome: " + pessoas.get(1).getNome());
        // busca do banco de dados
        System.out.println("Nome: " + pessoas.get(0).getNome());
        // já buscou esta pessoa... apenas retorna do cache...

        // A terceira pessoa nunca será consultada no banco de dados
        //(melhor performance - lazy loading)
        System.out.println("Id da 3ª - " + pessoas.get(2).getId());
        //pode imprimir o ID do objeto, e o proxy nao será inicializado.
    }
}
